import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class calc {
    public static void main(String[] args){
        try {
            Scanner BufferExpression = new Scanner(System.in);
            String Expression = BufferExpression.nextLine();
            Pattern PatternExpression = Pattern.compile("^\\-\\d|^\\-|\\((\\-\\d)\\)|\\((\\-\\d)|[0-9]*[.,]?[0-9]|[+\\-*/()]");
            List<String> Exp = new ArrayList<>();
            List<String> SubString;
            String[] Signs = new String[]{"/", "*", "-", "+"};
            Integer PrimaryValue = null;
            Integer FinalValue = null;
            Matcher MatcherExpression = PatternExpression.matcher(Expression);

            while (MatcherExpression.find()) Exp.add(MatcherExpression.group());

            if (!Expression.matches("[\\d+\\-*/()]+")) {
                System.out.println("Обнаружены недопустимые символы.");
                System.exit(0);
            }
            if (!CheckBracketQuantity(Exp)) {
                System.out.println("Пропущена скобка");
                System.exit(0);
            }

            while (Exp.contains("(") && Exp.contains(")")) {
                PrimaryValue = Exp.indexOf("(");
                FinalValue = Exp.indexOf(")")+1;

                for (int i = FinalValue - 1; i > PrimaryValue; i--) {
                    if (Exp.get(i).equals(")")) FinalValue = i + 1;

                }
                for (int i = PrimaryValue + 1; i < FinalValue; i++) {
                    if (Exp.get(i).equals("(")) PrimaryValue = i;
                }
                SubString = Exp.subList(PrimaryValue, FinalValue);
                SubString = CalculationInBrackets(SubString, Signs);
                String substringResult = SubString.get(0);
                SubString.clear();
                Exp.add(PrimaryValue, substringResult);
            }
            Exp = CalculationInBrackets(Exp, Signs);
            for (String res : Exp) {
                System.out.println(res);
            }
              }catch(Exception ex){
                System.out.println(ex.getMessage());
                System.exit(0);
            }

        }

    private static List CalculationInBrackets (List<String> substr, String[] signs){
        for (int j = 0; j < signs.length; j++) {
        for (int i = 0; i < substr.size(); i++) {
                if (substr.get(i).equals(signs[j])) {
                    String leftNumber = substr.get(i - 1);
                    String rightNumber = substr.get(i + 1);
                    String PrimaryLeftNumber = null;
                    String PrimaryRightNumber = null;
                    String sign = substr.get(i);
                    if (leftNumber.endsWith(")")) {
                        PrimaryLeftNumber = leftNumber;
                        leftNumber = leftNumber.replace('(', ' ');
                        leftNumber = leftNumber.replace(')', ' ');
                        leftNumber.trim();
                    }
                    if (rightNumber.endsWith(")")){
                        PrimaryRightNumber = rightNumber;
                        rightNumber = rightNumber.replace('(',' ');
                        rightNumber = rightNumber.replace(')',' ');
                        rightNumber.trim();
                }
                    Double result = Calculation(Double.parseDouble(leftNumber), Double.parseDouble(rightNumber), sign);

                        if(PrimaryLeftNumber!=null && PrimaryRightNumber==null) {
                            substr.remove(substr.indexOf(PrimaryLeftNumber));
                            substr.remove(substr.indexOf(rightNumber));
                            substr.remove(substr.indexOf(sign));
                        } else if (PrimaryRightNumber!=null && PrimaryLeftNumber==null) {
                        substr.remove(substr.indexOf(leftNumber));
                        substr.remove(substr.indexOf(PrimaryRightNumber));
                        substr.remove(substr.indexOf(sign));
                        } else if (PrimaryRightNumber!=null && PrimaryLeftNumber!=null) {
                            substr.remove(substr.indexOf(PrimaryLeftNumber));
                            substr.remove(substr.indexOf(PrimaryRightNumber));
                            substr.remove(substr.indexOf(sign));
                        }
                        else {
                            substr.remove(substr.indexOf(leftNumber));
                            substr.remove(substr.indexOf(rightNumber));
                            substr.remove(substr.indexOf(sign));
                        }

                    if (substr.isEmpty()) {
                        substr.add(result.toString());
                        i=0;
                    } else {
                        substr.add(i-1, result.toString());
                        i=0;
                    }
                }
            }
        }
        if(substr.contains("(") && substr.contains(")")) {
            substr.remove(substr.indexOf("("));
            substr.remove(substr.indexOf(")"));
        }
        return substr;
    }

    private static boolean CheckBracketQuantity (List <String> Expression){
        Integer CountLeftBracket = 0;
        Integer CountRightBracket = 0;
        for (String TestCountBracket:Expression){
            if (TestCountBracket.equals("(")) CountLeftBracket +=1;
            if (TestCountBracket.equals(")")) CountRightBracket +=1;
        }
        if (CountLeftBracket!=CountRightBracket) return false;
         else return true;
    }

    private static Double Calculation( Double firstNumber, Double secondNumber, String action){
        Double result = 0.0;
        switch (action){
            case ("+"):
                result = firstNumber+secondNumber;
                break;
            case ("-"):
                result = firstNumber-secondNumber;
                break;
            case ("*"):
                result = firstNumber*secondNumber;
                break;
            case ("/"):
                result = firstNumber/secondNumber;
                break;
        }
        return result;
    }
}
